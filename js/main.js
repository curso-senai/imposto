let preco;
let valorEntrada;
let valorJuros;

btnCadastrar.addEventListener('click', function () {

    preco = precoVeiculo.value;

    valorEntrada = calc(preco);
    valorJuros = juros(valorEntrada);
    valorParcelas = parcelar(60, valorJuros);
    
    entrada.innerHTML = `R$ ${valorEntrada}`;
    parcelas.innerHTML = `60x ${valorParcelas}`;
    view('result');
});

function calc(price) {
    return price / 2;
}

function parcelar(xvezes, valor){
    if(typeof xvezes !== 'number' || typeof valor !== 'number') return;

    return valor/xvezes;
}

function juros(value) {
    return value * 1.70;
}

function view(idName){
    document.getElementById(idName).style.display = "flex";
}

function changeBackground(){
    let randoBackg = Math.floor(Math.random() * 10);
    context.style.background = `url(../img/${randoBackg}.png)`;
}

// setInterval(changeBackground, 5000);